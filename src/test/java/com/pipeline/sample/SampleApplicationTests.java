package com.pipeline.sample;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;

@SpringBootTest
@Testcontainers
@Slf4j
class SampleApplicationTests {

    @Container
    private static PostgreSQLContainer postgresqlContainer = new PostgreSQLContainer()
            .withDatabaseName("postgres")
            .withUsername("postgres")
            .withPassword("password");

    @BeforeAll
    static void setUp() {
        System.setProperty("spring.datasource.url", "jdbc:postgresql://"+postgresqlContainer.getContainerIpAddress()+":" + String.valueOf(postgresqlContainer.getFirstMappedPort() + "/postgres"));
    }

    @Test
    void contextLoads() {
        Map<String, String> getenv = System.getenv();
        log.error("###################################################################");
        getenv.entrySet().forEach(e -> {log.error("key:"+e.getKey()+" value:"+e.getValue());});
        log.error("###################################################################");
    }

}
